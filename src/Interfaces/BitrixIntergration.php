<?php

namespace BitrixConnector\Interfaces;

use App\Helpers\B2BUrl;
use BitrixConnector\BitrixModule;

interface BitrixIntergration
{
    public function getBitrixModule(): BitrixModule;
}