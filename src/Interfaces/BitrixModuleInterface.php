<?php

namespace BitrixConnector\Interfaces;

use BitrixConnector\BitrixRecord;

interface BitrixModuleInterface
{
    public function list();
    public function get($id);
    public function add(array $fields);
    public function update($id, array $fields);
    public function delete($id);
    
    public function fields(): array;
    
    public function search(array $filters);
    public function find(array $filters);
}