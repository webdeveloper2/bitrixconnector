<?php

namespace BitrixConnector;

use Illuminate\Support\Collection;

class BitrixRecordProcessor
{
    protected $module;
    protected $records;
    
    public function __construct(BitrixModule $module)
    {
        $this->module = $module;
        $this->records = new Collection();
    }
    
    public function search($query = [])
    {
        $results = new Collection();
        
        $search = $this->module->search($query);
        
        while (isset($search->next)) {
            foreach ($search->result as $record)
                $results->push($record);
            
            $search = $this->module->search($query, $search->next);
        }
        
        $this->records = $results;
        
        return $this;
    }
    
    public function all()
    {
        $results = new Collection();

        $list = $this->module->list();
        
        while(isset($list->next)) {
            foreach ($list->result as $record) {
                $results->push($record);
            }
            $list = $this->module->list($list->next);
        }

        $this->records = $results;

        return $this;
    }
    
    public function getRecords(): Collection
    {
        return $this->records;
    }
    
    public function update($fields)
    {
        $total = $this->records->count();
        
        foreach ($this->records as $i => $record) {
            $count = $i + 1;
            $response = $this->module->update($record->ID, $fields);

            if ($response->result) {
                $moduleName = $this->module->getBitrixModuleName();
                print ("[$count/$total] Updated `$moduleName` bitrix record {$record->ID}\n");
            }
        }
    }

    public function reassign()
    {
        $total = $this->records->count();

        foreach ($this->records as $i => $record) {
            $count = $i + 1;
            $response = $this->module->update($record->ID, ['ASSIGNED_BY_ID' => $record->ASSIGNED_BY_ID]);

            if ($response->result) {
                $moduleName = $this->module->getBitrixModuleName();
                print ("[$count/$total] Reassigned `$moduleName` bitrix record {$record->ID}\n");
            }
        }
    }
    
    public function delete()
    {
        $total = $this->records->count();

        foreach ($this->records as $i => $record) {
            $count = $i + 1;
            $response = $this->module->delete($record->ID);
            
            if ($response->result) {
                $moduleName = $this->module->getBitrixModuleName();
                print ("[$count/$total] Deleted `$moduleName` bitrix record {$record->ID}\n");
            }
        }
    }
}