<?php

namespace BitrixConnector;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Database\Eloquent\Model;

class BitrixChat
{
    private $client;
    private $webhookUrl;
    
    public $chat;
    
    public function __construct(int $chatId)
    {
        $this->webhookUrl = config('bitrix.webhooks.chat');
        $this->client = new Client(['verify' => false]);
        $this->chat = $chatId;
    }
    
    public function sendMessage($message)
    {
        return $this->request('post', 'im.message.add', [
            RequestOptions::JSON => [
                "CHAT_ID" => $this->chat,
                "MESSAGE" => $message,
                "SYSTEM" => "N"
            ]
        ]);
    }
    
    public function request($method, $chatAction, $options)
    {
        $url = "{$this->webhookUrl}/$chatAction";
        
        $response = $this->client->request($method, $url, $options);

        return json_decode($response->getBody()->getContents());
    }

    public static function report(string $message, Model $entry = null, $url = null, $options = null)
    {
        $bitrixChat = new BitrixChat(config('bitrix.chat.reports'));

        $url = preg_replace('/([\w\d]{50}+)/m', 'xxx', $url);

        if ($entry) {
            $code = $entry->sap_code ?? $entry->sap_id ?? $entry->code;
            $company = optional($entry->company)->title;
            
            if ($code && $company) {
                $url = $entry->getB2BUrl()->getAbsoluteUrl();
                $bitrixChat->sendMessage("[B][$company: $code][/B]: [URL=$url]{$entry}[/URL] [BR] [I]{$message}[/I] [BR] $url");
            }
            else {
                $bitrixChat->sendMessage("[B][$entry][/B]: [BR] [I]{$message}[/I] [BR] $url");
            }
        } else {
            $bitrixChat->sendMessage("[I]{$message}[/I] [BR] $url");
        }
    }
}