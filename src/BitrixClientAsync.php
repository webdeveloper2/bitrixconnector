<?php

namespace BitrixConnector;

use BitrixConnector\Exceptions\BitrixRecordNotFoundException;
use GuzzleHttp\Psr7\Request;

class BitrixClientAsync extends BitrixClient
{
    public function insert($fields = []): Request
    {
        return $this->bitrixModule->add(array_merge($this->prepareFields(), $fields), true);
    }
    
    public function update($fields = []): Request
    {
        return $this->bitrixModule->update($this->ID, $fields ?: $this->prepareFields(), true);
    }
    
    public function delete(): Request
    {
       $this->initializeDBMappings();
       $bitrixRecord = $this->bitrixRecord;
       
       if (!$bitrixRecord)
           throw new BitrixRecordNotFoundException($this->bitrixModule, $this->b2bModelInstance);

        $request = $this->bitrixModule->delete($this->ID, true);
       
       $this->bitrixMappingQuery->delete();
       
       return $request;
    }
}