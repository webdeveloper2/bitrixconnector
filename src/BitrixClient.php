<?php

namespace BitrixConnector;

use BitrixConnector\Exceptions\BitrixDBMappingAlreadyExistsException;
use BitrixConnector\Exceptions\BitrixRecordAlreadyExists;
use BitrixConnector\Exceptions\BitrixFieldDoesNotExistsException;
use BitrixConnector\Exceptions\BitrixMappingNotFoundException;
use BitrixConnector\Exceptions\BitrixRecordNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class BitrixClient
{
    protected $b2bModelInstance;
    protected $bitrixModule;
    protected $bitrixFieldMapping;
    protected $createBitrixRecordIfNotExists;
    
    public $bitrixMappingQuery;
    public $bitrixMapping = [];
    public $bitrixRecord = null;
    
    private $mapped = false;
    
    public function __construct(Model $b2bModelInstance, $createBitrixRecordIfNotExists = true)
    {
        $this->b2bModelInstance = $b2bModelInstance;
        $this->bitrixModule = $b2bModelInstance->getBitrixModule();
        $this->createBitrixRecordIfNotExists = $createBitrixRecordIfNotExists;
    }
    
    public function get($id)
    {
        $record = $this->bitrixModule->get($id);
        
        if (!$record->result)
            throw new BitrixRecordNotFoundException($this->bitrixModule, $this->b2bModelInstance);
        
        return new BitrixRecord($record);
    }
    
    public function getBitrixRecord()
    {
        $this->initializeDBMappings();
        
        return $this->bitrixRecord;
    }
    
    public function insert($fields = [])
    {
        $this->initializeDBMappings();
        $this->initializeFieldMappings();
        
        if ($this->bitrixMapping) {
            throw new BitrixRecordAlreadyExists($this->bitrixModule, $this->bitrixRecord, $this->b2bModelInstance);
        }
        
        $response = $this->bitrixModule->add(array_merge($this->prepareFields(), $fields));
        
        try {
            self::updateMapping($this->bitrixModule->getBitrixModuleName(), $response->result, $this->b2bModelInstance->getKey());
        } 
        catch (BitrixDBMappingAlreadyExistsException $e) {
            $client = app('sentry');
            $client->captureException($e);
        }
        
        return $response;
    }
    
    public function create($fields = [])
    {
        try {
            $response = $this->insert($fields);
        }
        catch (BitrixRecordAlreadyExists $e) {
            $bitrixModule = $e->getBitrixModule();
            $bitrixRecord = $e->getBitrixRecord();
            $b2bInstance = $e->getB2BInstance();
            $primaryKey = $bitrixModule->getPrimaryKey();
            
            if (!$this->bitrixMapping) {
                try {
                    self::updateMapping($bitrixModule->getBitrixModuleName(), $bitrixRecord->$primaryKey, $b2bInstance->getKey());
                }
                catch (BitrixDBMappingAlreadyExistsException $e) {
                    $client = app('sentry');
                    $client->captureException($e);
                }
            }
            
            return $bitrixRecord;
        }
        
        return $this->get($response->result);
    }
    
    public function update($fields = [])
    {
        $this->initializeDBMappings();
        $this->initializeFieldMappings();

        if (!$this->bitrixMapping)
            throw new BitrixMappingNotFoundException($this->b2bModelInstance);
        
        $response = $this->bitrixModule->update($this->ID, $fields ?: $this->prepareFields());
        
        if ($response->result) {
            $updated_at = Carbon::parse($response->time->date_finish);
            $this->bitrixMappingQuery->update(['updated_at' => $updated_at->format('Y-m-d H:i:s')]);
        }
        
        return $response;
    }
    
    public function delete()
    {
        $response = $this->bitrixModule->delete($this->ID);
        
        $this->bitrixMappingQuery->delete();
        
        return $response;
    }

    public function getOrCreate($fields = []): BitrixRecord
    {
        try {
            return $this->bitrixRecord ?: $this->create($fields);
        }
        catch (BitrixRecordAlreadyExists $e) {
            return $e->getBitrixRecord();
        }
    }
    
    public function updateOrCreate($fields = []): BitrixRecord
    {
        try {
            $this->update($fields);
            return $this->get($this->ID);
        }
        catch (BitrixMappingNotFoundException $e) {
            return $this->create($fields);
        }
        catch (BitrixRecordNotFoundException $e) {
            return $e->getB2BInstance()->bitrix->create($fields);
        }
    }
    
    public function createOrUpdate($fields = []): BitrixRecord
    {
        try {
            return $this->create($fields);
        }
        catch (BitrixRecordAlreadyExists $e) {
            $this->update($fields);
            return $e->getBitrixRecord();
        }
    }
    
    public static function updateMapping($bitrixModuleName, $bitrixId, $b2bId)
    {
        try {
            DB::table('bitrix_module_mappings')->insert([
                'bitrix_module' => $bitrixModuleName,
                'bitrix_id' => $bitrixId,
                'b2b_id' => $b2bId,
            ]);
        } catch (\PDOException $e) {
            if ($e->getCode() == 23000) {
                throw new BitrixDBMappingAlreadyExistsException($bitrixModuleName, $bitrixId, $b2bId);
            }
        }
    }
    
    public static function deleteMapping($bitrixModuleName, $bitrixId, $b2bId)
    {
        DB::table('bitrix_module_mappings')->where([
            'bitrix_module' => $bitrixModuleName,
            'bitrix_id' => $bitrixId,
            'b2b_id' => $b2bId,
        ])->delete();
    }
    
    public function initializeDBMappings()
    {
        $this->bitrixMappingQuery = DB::table('bitrix_module_mappings')
            ->where('bitrix_module', $this->bitrixModule->getBitrixModuleName())
            ->where('b2b_id', $this->b2bModelInstance->getKey());

        $this->bitrixMapping = $this->bitrixMappingQuery->first();

        if ($this->bitrixMapping) {
            try {
                $this->bitrixRecord = $this->get($this->bitrixMapping->bitrix_id);
                $this->mapped = true;
            } catch (BitrixRecordNotFoundException $e) {
                $this->bitrixMapping = false;
                $this->mapped = false;
                // Delete old mapping
                $this->bitrixMappingQuery->delete();
            }
        }
    }
    
    public function initializeFieldMappings()
    {
        $this->bitrixFieldMapping = $this->bitrixModule->getBitrixFieldMapping();
    }
    
    protected function prepareFields()
    {
        if (!$this->mapped) {
            $this->initializeDBMappings();
        }

        $this->initializeFieldMappings();

        $fields = [];
        $bitrixFields = $this->getBitrixFields();
        
        foreach ($this->bitrixFieldMapping as $field => $value) {
            if (!array_key_exists($field, $bitrixFields))
                throw new BitrixFieldDoesNotExistsException($field);
            
            $fields[$field] = $value;
        }
        
        return $fields;
    }
    
    protected function getBitrixFields()
    {
        $moduleName = $this->bitrixModule->getBitrixModuleName();
        
        return Cache::remember("bitrix:$moduleName:fields", 60, function() {
            return $this->bitrixModule->fields()['result'];
        });
    }
    
    public function __get($name)
    {
        if (!$this->mapped)
            $this->initializeDBMappings();

        $bitrixRecord = $this->createBitrixRecordIfNotExists ? $this->getOrCreate() : $this->bitrixRecord;
        
        if (!$bitrixRecord)
            throw new BitrixRecordNotFoundException($this->bitrixModule, $this->b2bModelInstance);
        
        return $bitrixRecord->$name;
    }
}