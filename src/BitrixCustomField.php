<?php

namespace BitrixConnector;

use Illuminate\Support\Facades\DB;

class BitrixCustomField
{
    protected $fieldName;
    protected $b2bId;
    protected $mapping;
    
    public function __construct(string $fieldName, $b2bId)
    {
        $this->fieldName = $fieldName;
        $this->b2bId = $b2bId;
        
        $this->mapping = DB::table('bitrix_custom_field_mappings')
            ->where('field_name', $fieldName)
            ->whereIn('b2b_id', is_array($b2bId) ? $b2bId : [$b2bId])
            ->get();
        
        // TODO: report if no mapping is found
    }
    
    public function getBitrixId()
    {
        if ($this->mapping->count() > 1)
            return $this->mapping->pluck('bitrix_id');
        
        return optional($this->mapping->first())->bitrix_id;
    }
}