<?php

namespace BitrixConnector\Exceptions;

class BitrixNoRecordsFoundException extends BitrixException {}