<?php

namespace BitrixConnector\Exceptions;

use Illuminate\Database\Eloquent\Model;
use Throwable;

class BitrixMappingAlreadyExistsException extends BitrixClientException 
{
    public function __construct(Model $b2bModelInstance, $message = "", $code = 0, Throwable $previous = null)
    {
        $b2bTable = $b2bModelInstance->getTable();
        $b2bId = $b2bModelInstance->getKey();
        
        $message = "A bitrix mapping already exists for b2b `$b2bTable` id:$b2bId";
            
        parent::__construct($b2bModelInstance, $message, $code, $previous);
    }
}