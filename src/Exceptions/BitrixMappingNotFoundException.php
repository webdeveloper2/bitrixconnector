<?php

namespace BitrixConnector\Exceptions;

use BitrixConnector\BitrixModule;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class BitrixMappingNotFoundException extends BitrixClientException 
{
    protected $b2bModelInstance;
    
    public function __construct(Model $b2bModelInstance, $message = "", $code = 0, Throwable $previous = null)
    {
        $b2bTable = $b2bModelInstance->getTable();
        $b2bId = $b2bModelInstance->getKey();
        
        $message = "No bitrix mapping was found for b2b `$b2bTable` id:$b2bId";
        
        parent::__construct($b2bModelInstance, $message, $code, $previous);
    }
}