<?php

namespace BitrixConnector\Exceptions;

use Throwable;

class BitrixDBMappingAlreadyExistsException extends BitrixException 
{
    public function __construct($bitrixModuleName, $bitrixId, $b2bId,  $message = "", $code = 0, Throwable $previous = null)
    {
        $message = "A mapping already exists for `$bitrixModuleName` with bitrix_id:$bitrixId and b2b_id:$b2bId";
        
        parent::__construct($message, $code, $previous);
    }
}