<?php

namespace BitrixConnector\Exceptions;

use BitrixConnector\BitrixModule;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class BitrixRecordNotFoundException extends BitrixRequestException 
{
    protected $bitrixModule;
    protected $bitrixRecord;
    protected $b2bInstance;
    
    public function __construct(BitrixModule $bitrixModule, Model $b2bInstance, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->bitrixModule = $bitrixModule;
        $this->b2bInstance = $b2bInstance;
        
        $bitrixModuleName = $bitrixModule->getBitrixModuleName();
        $message = "Bitrix record was not found for module `$bitrixModuleName` [$b2bInstance:{$b2bInstance->id}]";
        
        parent::__construct($b2bInstance, $message, $code, $previous);
    }
    
    public function getBitrixModule()
    {
        return $this->bitrixModule;
    }
    
    public function getB2BInstance()
    {
        return $this->b2bInstance;
    }
}