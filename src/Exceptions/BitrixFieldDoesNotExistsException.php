<?php

namespace BitrixConnector\Exceptions;

use Throwable;

class BitrixFieldDoesNotExistsException extends BitrixException 
{
    public function __construct(string $bitrixField, $message = "", $code = 0, Throwable $previous = null)
    {
        $message = sprintf("Bitrix field `%s` does not exist", $bitrixField);
        
        parent::__construct($message, $code, $previous);
    }
}