<?php

namespace BitrixConnector\Exceptions;

use BitrixConnector\BitrixModule;
use BitrixConnector\BitrixRecord;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class BitrixRecordAlreadyExists extends BitrixRequestException 
{
    protected $bitrixModule;
    protected $bitrixRecord;
    protected $b2bInstance;
    
    public function __construct(BitrixModule $bitrixModule, BitrixRecord $bitrixRecord, Model $b2bInstance, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->bitrixModule = $bitrixModule;
        $this->bitrixRecord = $bitrixRecord;
        $this->b2bInstance = $b2bInstance;
        
        $message = "Bitrix Record for $b2bInstance already exists with bitrix_id: {$bitrixRecord->ID} and b2b_id {$b2bInstance->id}";
        
        parent::__construct($message, $code, $previous);
    }
    
    public function getBitrixModule()
    {
        return $this->bitrixModule;
    }
    
    public function getBitrixRecord()
    {
        return $this->bitrixRecord;
    }
    
    public function getB2BInstance()
    {
        return $this->b2bInstance;
    }
}