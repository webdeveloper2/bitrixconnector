<?php

namespace BitrixConnector\Exceptions;

use BitrixConnector\BitrixRecord;
use Throwable;

class BitrixDepartmentAlreadyExistsException extends BitrixException 
{
    protected $department;
    
    public function __construct(BitrixRecord $department, $code = 0, Throwable $previous = null)
    {
        $this->department = $department;
        
        $message = "Bitrix Department with bitrix ID: {$department->ID} already exists";
        
        parent::__construct($message, $code, $previous);
    }
    
    public function getDepartment()
    {
        return $this->department;
    }
}