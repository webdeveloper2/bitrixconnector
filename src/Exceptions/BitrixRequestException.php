<?php

namespace BitrixConnector\Exceptions;

use BitrixConnector\BitrixChat;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class BitrixRequestException extends BitrixException 
{
    public function __construct(Model $entry = null, $message = "", $url = null, $options = null, $code = 0, Throwable $previous = null)
    {
        BitrixChat::report($message, $entry, $url, $options);

        parent::__construct($message, $code, $previous);
    }
    
}