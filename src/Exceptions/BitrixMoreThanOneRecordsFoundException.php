<?php

namespace BitrixConnector\Exceptions;

class BitrixMoreThanOneRecordsFoundException extends BitrixException {}