<?php

namespace BitrixConnector\Exceptions;

use Illuminate\Database\Eloquent\Model;
use Throwable;

class BitrixClientException extends BitrixException
{
    protected $b2bModelInstance;
    protected $bitrixModule;
    
    public function __construct(Model $b2bModelInstance, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->b2bModelInstance = $b2bModelInstance;
        $this->bitrixModule = $b2bModelInstance->getBitrixModule();
        
        parent::__construct($message, $code, $previous);
    }

    public function getB2BModelInstance()
    {
        return $this->b2bModelInstance;
    }
    
    public function getBitrixModule()
    {
        return $this->bitrixModule;
    }

}