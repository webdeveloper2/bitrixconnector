<?php

namespace BitrixConnector\Commands;

use BitrixConnector\BitrixBatchProcessor;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;

abstract class BitrixCommand extends Command
{
    abstract function getB2BModelClass(): string;
    abstract function getBitrixModuleName(): string;
    
    protected $b2bModelClass;
    protected $bitrixModuleName;
    public $processor;
    
    public function __construct()
    {
        $this->b2bModelClass = $this->getB2BModelClass();
        $this->bitrixModuleName = $this->getBitrixModuleName();
        $this->processor = new BitrixBatchProcessor($this->bitrixModuleName, $this);
        
        $this->signature = "bitrix:{$this->bitrixModuleName} {action} {--multiple=} {--limit=}";
        
        parent::__construct();
    }

    public function handle()
    {
        $multiple = (int) $this->option('multiple');

        $query = $this->getQuery();
        
        $limit = $this->option('limit');

        if (!is_null($limit)) {
            $query->limit($limit);
        }
        
        $mappingSubQuery = function($query) {
            $this->getMappingSubQuery($query);
        };
        
        switch ($this->argument('action')) {
            case 'insert':
                $this->insert($query, $mappingSubQuery, $multiple);
            break;
            case 'update':
                $this->update($query, $mappingSubQuery, $multiple);
            break;
            case 'delete':
                $this->delete($query, $mappingSubQuery, $multiple);
            break;
            case 'update_or_create':
                $this->updateOrCreate($query, $mappingSubQuery, $multiple);
            break;
        }
    }
    
    public function getQuery(): EloquentBuilder
    {
        $model = $this->b2bModelClass;
        
        return $model::query();
    }
    
    public function getMappingSubQuery($query): QueryBuilder
    {
        return $query->select('b2b_id')
            ->from('bitrix_module_mappings')
            ->where('bitrix_module', $this->bitrixModuleName);
    }
    
    public function insert($query, $mappingSubQuery, $multiple)
    {
        $query = $query->whereNotIn('id', $mappingSubQuery);
        $this->process($query, 'insert', $multiple);
    }

    public function update($query, $mappingSubQuery, $multiple)
    {
        $query = $query->whereIn('id', $mappingSubQuery);
        $query = $query->whereNotIn('id', DB::table('bitrix_updates')->where('bitrix_module', $this->getBitrixModuleName())->pluck('b2b_id'));
        $this->process($query, 'update', $multiple);
    }

    public function delete($query, $mappingSubQuery, $multiple)
    {
        $query = $query->whereIn('id', $mappingSubQuery);
        $this->process($query, 'delete', $multiple);
    }
    
    public function updateOrCreate($query, $mappingSubQuery, $multiple)
    {
        $query = $query->whereIn('id', $mappingSubQuery);
        $this->process($query, 'updateOrCreate', $multiple);
    }
    
    public function process(Builder $query, $action, $multiple, $callback = null)
    {
        $this->processor->process($query, $action, $multiple, $callback);
    }
}