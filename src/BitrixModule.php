<?php

namespace BitrixConnector;

use BitrixConnector\Exceptions\BitrixException;
use BitrixConnector\Exceptions\BitrixRecordNotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\RequestException;
use BitrixConnector\Interfaces\BitrixModuleInterface;
use BitrixConnector\Exceptions\BitrixRequestException;
use BitrixConnector\Exceptions\BitrixRecordAlreadyExists;
use BitrixConnector\Exceptions\BitrixNoRecordsFoundException;
use BitrixConnector\Exceptions\BitrixMoreThanOneRecordsFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class BitrixModule implements BitrixModuleInterface
{
    abstract public function getWebhookUrl(): string;

    abstract public function getBitrixModuleName(): string;

    protected static $primaryKey = 'ID';
    protected static $searchFields = [];

    protected $b2bInstance;

    protected $webhookUrl;
    protected $bitrixModuleName;
    protected $fieldMapping;

    protected $client;

    protected $json;

    public function __construct(Model $b2bInstance = null, bool $json = false)
    {
        $this->b2bInstance = $b2bInstance;
        $this->json = $json;

        $this->webhookUrl = $this->getWebhookUrl();
        $this->bitrixModuleName = $this->getBitrixModuleName();

        $this->client = new Client(['verify' => false]);
    }

    public function getPrimaryKey()
    {
        return static::$primaryKey;
    }

    public function list($start = 0)
    {
        return $this->request('get', 'list', [
            RequestOptions::QUERY => ['start' => $start]
        ]);
    }

    public function add(array $fields, $async = false)
    {
        if ($this->json) {
            $params = [
                RequestOptions::JSON => $fields
            ];
        } else {
            $params = [
                RequestOptions::JSON => [
                    'fields' => $fields
                ]
            ];
        }

        return $this->request('post', 'add', $params, $async);
    }

    public function get($id)
    {
        $response = $this->request('get', 'get', [
            RequestOptions::QUERY => [
                'ID' => $id
            ]
        ]);
        
        if (!$response->result) {
            if ($this->b2bInstance)
                throw new BitrixRecordNotFoundException($this->b2bInstance->getBitrixModule(), $this->b2bInstance);
            
            throw new BitrixException("Bitrix record was not found for `{$this->bitrixModuleName}` with id:$id");
        }
        
        return $response;
    }

    public function update($id, array $fields, $async = false)
    {
        if ($this->json) {
            $params = [
                RequestOptions::JSON => array_merge(['id' => $id], $fields)
            ];
        } else {
            $params = [
                RequestOptions::JSON => [
                    'id' => $id,
                    'fields' => $fields
                ]
            ];
        }

        return $this->request('post', 'update', $params, $async);
    }

    public function delete($id, $async = false)
    {
        return $this->request('post', 'delete', [
            RequestOptions::QUERY => [
                'ID' => $id
            ]
        ], $async);
    }

    public function search(array $filters, $start = 0)
    {
        return $this->request('post', 'list', [
            RequestOptions::JSON => [
                'start' => $start,
                'filter' => $filters
            ]
        ]);
    }

    public function find(array $filters)
    {
        $search = $this->search($filters);
        
        if (empty($search->results))
            return false;
        
        if (!isset($search->total))
            return false;

        if ($search->total === 0)
            throw new BitrixNoRecordsFoundException();

        if ($search->total > 1)
            throw new BitrixMoreThanOneRecordsFoundException();

        return new BitrixRecord($search->result[0]);
    }

    public function fields(): array
    {
        return (array)$this->request('get', 'fields');
    }

    public function request($type, $action, $options = [], $async = false)
    {
        try {
            $url = "{$this->webhookUrl}/{$this->bitrixModuleName}.{$action}";
            if ($this->json && ($action === 'add' || $action === 'update')): $url .= '.json'; endif;

            if ($async) {
                $options = array_key_exists(RequestOptions::JSON, $options) ? $options[RequestOptions::JSON] : $options[RequestOptions::QUERY];
                return new Request($type, $url, ['Content-Type' => 'application/json'], json_encode($options));
            }

            // print("$url \n");

            $requestType = $async ? 'requestAsync' : 'request';
            $response = $this->client->$requestType($type, $url, $options);
            $results = json_decode($response->getBody()->getContents());
        }
        catch (RequestException $e) {
            if (!$e->getResponse()) {
                throw new BitrixRequestException($this->b2bInstance, "Empty server response: ". $e->getMessage(), $url, $options);
            }
            
            $results = json_decode($e->getResponse()->getBody()->getContents(), true);

            if (isset($results['error'])) {
                $errorDescription = strip_tags($results['error_description']);

                if (Str::contains($errorDescription, 'already exists')) {
                    $searchFields = $this->json ? $options[RequestOptions::JSON] : $options[RequestOptions::JSON]['fields'];
                    
                    if (!empty(static::$searchFields)) {
                        $searchFields = array_intersect_key($searchFields, array_flip(static::$searchFields));
                    }

                    $record = $this->find($searchFields);
                    
                    if ($record) {
                        if ($this->b2bInstance)
                            throw new BitrixRecordAlreadyExists($this, $record, $this->b2bInstance, $errorDescription);
                        
                        throw new BitrixRequestException($this->b2bInstance, "Bitrix record already exists for `{$this->bitrixModuleName}` with id:{$record->ID} ($errorDescription)", $url, $options);
                    }
                }

                if (Str::contains($errorDescription, 'Not found')) {
                    if ($this->b2bInstance)
                        throw new BitrixRecordNotFoundException($this, $this->b2bInstance, $errorDescription);

                    throw new BitrixRequestException($this->b2bInstance, "Bitrix record was not found for `{$this->bitrixModuleName}` ($errorDescription)", $url, $options);
                }

                // var_dump($url, $type, $options);

                throw new BitrixRequestException($this->b2bInstance, $errorDescription, $url, $options);
            }
        }

        return $results;
    }
}