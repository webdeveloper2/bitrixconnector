<?php

namespace BitrixConnector;

use BitrixConnector\Exceptions\BitrixDBMappingAlreadyExistsException;
use BitrixConnector\Exceptions\BitrixRecordAlreadyExists;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\DB;

class BitrixBatchProcessor
{
    protected $bitrixModuleName;
    private $command;
    
    public function __construct($bitrixModuleName, $command = null)
    {
        $this->bitrixModuleName = $bitrixModuleName;
        $this->command = $command;
    }

    public function process($query, $action, $multiple = false, $callback = null)
    {
        $actionName = ucfirst($action);
        $total = $query->count();
        $entriesRepository = $query->pluck('id', 'id');
        
        // print ("Total records: $total\n");

        $query->chunk(100, function($chuck) use ($action, $actionName, $multiple, &$total, &$entriesRepository, $callback) {
            $entries = $chuck->keyBy('id');

            if ($multiple) {
                $requests = function ($entries) use ($action) {
                    foreach ($entries as $entry_id => $entry) {
                        try {
                            yield $entry_id => $entry->bitrixAsync->$action();
                        } catch (\Exception $e) {
                            print ($entry_id . ": ".$e->getMessage() ."\n");
                        }
                    }
                };

                $client = new Client(['verify' => false]);

                $pool = new Pool($client, $requests($entries), [
                    'concurrency' => $multiple,
                    'fulfilled' => function (Response $response, $entry_id) use ($entries, $total, $action, $actionName, &$entriesRepository, $callback) {
                        $result = json_decode($response->getBody()->getContents());
                        $entry = $entries->get($entry_id);
                        try {
                            switch ($action) {
                                case 'insert': BitrixClient::updateMapping($this->bitrixModuleName, $result->result, $entry_id); break;
                                case 'delete': BitrixClient::deleteMapping($this->bitrixModuleName, $result->result, $entry_id); break;
                            }
                        }
                        catch (BitrixDBMappingAlreadyExistsException $e) {
                            $message = $e->getMessage();
                            print("$message\n");
                        }

                        if ($action === 'update')
                            self::update($entry);

                        // Callback function after success
                        if ($callback && is_callable($callback)) {
                            $callback($entry);
                        }

                        $entriesRepository->forget($entry_id);
                        $remaining = $total - $entriesRepository->count();
                        
                        print("$remaining/$total $actionName `{$this->bitrixModuleName}` \"$entry\" with b2b_id:{$entry->id}\n");
                    },
                    'rejected' => function (RequestException $e, $entry_id) use (&$entriesRepository, $actionName, $entries, $total) {
                        $entry = $entries->get($entry_id);
                        $entriesRepository->forget($entry_id);
                        $remaining = $total - $entriesRepository->count();
                        $message = $e->getMessage();
                        print("$remaining/$total $actionName `{$this->bitrixModuleName}` $entry: $message\n");
                    },
                ]);

                $promise = $pool->promise();
                $promise->wait();
            }
            else {
                $entriesIterator = function($entries) use (&$entriesRepository, $total, $actionName, $action, $callback) {
                    foreach ($entries as $entry_id => $entry) {
                        yield $entry_id => function() use ($entry, $entry_id, &$entriesRepository, $total, $actionName, $action, $callback) {
                            $entriesRepository->forget($entry_id);
                            $remaining = $total - $entriesRepository->count();
                            
                            print("$remaining/$total $actionName `{$this->bitrixModuleName}` \"$entry\" with b2b_id:{$entry->id}\n");
                            
                            $entry->bitrix->$action();

                            if ($action === 'update')
                                self::update($entry);
                            
                            if ($callback && is_callable($callback)) {
                                $callback($entry);
                            }
                        };
                    }
                };

                foreach ($entriesIterator($entries) as $entry_id => $iterator) {
                    try {
                        $iterator();
                    }
                    catch (BitrixRecordAlreadyExists $e) {
                        print ($e->getMessage(). "\n");
                    }
                    catch (\Exception $e) {
                        $client = app('sentry');
                        $client->captureException($e);
                        $message = $e->getMessage();
                        print("$message\n");
                    }
                }

            }
        });
    }
    
    public static function update($entry)
    {
        DB::table('bitrix_updates')->updateOrInsert(
            [
                'bitrix_module' => $entry->getBitrixModule()->getBitrixModuleName(),
                'b2b_id' => $entry->id
            ], [
                'updated_at' => Carbon::now()
            ]
        );
    }
    
}