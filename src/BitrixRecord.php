<?php

namespace BitrixConnector;

use BitrixConnector\Exceptions\BitrixFieldDoesNotExistsException;

class BitrixRecord
{
    public $attrs;
    
    public function __construct(object $attrs)
    {
        if (isset($attrs->result)) {
            if (isset($attrs->total) && $attrs->total === 1 && count($attrs->result) === 1) {
                $attrs = $attrs->result[0];
            } else {
                $attrs = $attrs->result;
            }
        }
        
        $this->attrs = $attrs;
    }

    public function __get($name)
    {
        if (!isset($this->attrs->$name)) {
            throw new BitrixFieldDoesNotExistsException($name);
        }
            
        return $this->attrs->$name;
    }
}