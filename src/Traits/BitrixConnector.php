<?php

namespace BitrixConnector\Traits;

use BitrixConnector\BitrixClient;
use BitrixConnector\BitrixClientAsync;

trait BitrixConnector
{
    public static function bootBitrixConnector()
    {
        static::retrieved(function ($model) {
            $model->bitrix = new BitrixClient($model);
            $model->bitrixAsync = new BitrixClientAsync($model);
        });
    }
}