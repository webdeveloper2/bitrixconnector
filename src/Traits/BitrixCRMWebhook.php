<?php

namespace BitrixConnector\Traits;

trait BitrixCRMWebhook
{
    public function getWebhookUrl(): string
    {
        return config('bitrix.webhooks.crm');
    }
}